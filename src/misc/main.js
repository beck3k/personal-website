var text = [
  "A Computer Programmer", 
  "An Electronics Enthusiast", 
  "A cellist", 
  "A full stack developer", 
  "The Co-president of FIRST robotics competition team 461"
];
function type(text, done, speed) {
    // console.log(text);
    $('#characteristic-typing').text(text.substring(0, $('#characteristic-typing').text().length + 1));
    $('#characteristic-typing').append(`<span aria-hidden="true" class="typing-bar"></span>`);
    if(text.length > $('#characteristic-typing').text().length){
      console.log()
      setTimeout(() => {
        type(text, done);
      }, speed);
    } else {
      done();
    }
}

function backspace(done) {
  $('#characteristic-typing').text($('#characteristic-typing').text().substring(0, $('#characteristic-typing').text().length - 1));
  $('#characteristic-typing').append(`<span aria-hidden="true" class="typing-bar"></span>`);
  if($('#characteristic-typing').text().length > 0){
    setTimeout(() => {
      backspace(done);
    }, 30);
  } else {
    done();
  }
}

function loopType(i, speed, delay, complete){
  txt = text[i];
  // console.log(i);
  type(txt, () => {
    setTimeout(() => {
      backspace(() => {
        console.log("done");
        if(i < text.length - 1) {
          loopType(i+1, speed*0.9, delay/2, complete);
        }else {
          complete();
        }
      });
    }, delay);
  }, speed);
}

  $(document).ready(() => {
    // type(text[0]);
    $('#characteristic-typing').text("");
    loopType(0, 100, 2000, () => {
      setTimeout(() => {
        type("Beck", ()=>{

        }, 100);
      }, 3000);
    });
  });

$(document).on('scroll', function() {
  console.log('scroll top', $(this).scrollTop());
  console.log('nav', ($('.typing-pane').position().top + $('.typing-pane').height()));
  if($(this).scrollTop() == $('nav').position().top){
    $('nav').removeClass("small-navbar");
  }else {
    $('nav').addClass("small-navbar");
  }

  if($(this).scrollTop() < ($('.typing-pane').position().top + $('.typing-pane').height())){
    $('.sidebar-nav').removeClass("stick");
  }else {
    $('.sidebar-nav').addClass("stick");
  }
})